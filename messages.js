const express=require('express');
const db = require('./fileDb');

const router = express.Router();


router.get('/',(req,res)=>{
    res.send(db.getItems())
});


router.post('/',(req,res)=>{
    const  date=new Date().toISOString();
    const message={
        data: date,
        text: req.body.message,
    };
    fs.writeFileSync(`./messages/${date}.txt`,JSON.stringify(message));

    res.send(message);

});


module.exports = router;
